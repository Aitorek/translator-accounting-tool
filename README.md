Built with Python 3.7 and PyQt5

Works with words/day and hours/day. For now, earnings are calculated only in euros. 

# To do
- [ ] Change colors 
- [ ] Add the option to use dollars, maybe more currencies
- [ ] Calculate Paypal taxes
- [ ] Add tab navigation between cells



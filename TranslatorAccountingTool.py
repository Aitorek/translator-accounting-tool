import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from PyQt5.QtWidgets import (QWidget, QPushButton, QApplication, QTextEdit, QLabel)
import sys

class Parse_GUI(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        w_color = "background-color: #F2E3BC;"
        tab_color = "background-color: #96BBBB;"

        #Window shape and color
        self.setGeometry(300, 300, 800, 500)
        self.setWindowTitle('Translator Accounting Tool')
        self.setStyleSheet(w_color)

        #Button
        self.btn = QPushButton('Calculate', self)
        self.btn.setShortcut('Ctrl+Q')
        self.btn.setStyleSheet(tab_color)
        self.btn.move(20, 22)
        self.btn.clicked.connect(self.buttonClicked)

        # Text boxes
            # euros per word
        self.leuros = QLabel(self)
        self.leuros.setText("€/word\n or hour")
        self.leuros.setGeometry(20, 100, 150, 50)

        self.weuros = QTextEdit(self)
        self.weuros.setStyleSheet(tab_color)
        self.weuros.setGeometry(100, 100, 150, 50)

            # daily output
        self.loutput = QLabel(self)
        self.loutput.setText("Daily\nOutput")
        self.loutput.setGeometry(20, 170, 150, 50)

        self.woutput = QTextEdit(self)
        self.woutput.setStyleSheet(tab_color)
        self.woutput.setGeometry(100, 170, 150, 50)

            # Days in month
        self.ldays_month = QLabel(self)
        self.ldays_month.setText("Days in\n month")
        self.ldays_month.setGeometry(20, 230, 50, 50)

        self.wdays_month = QTextEdit(self)
        self.wdays_month.setStyleSheet(tab_color)
        self.wdays_month.setGeometry(100, 230, 50, 50)

            # Savings
        self.lsavings = QLabel(self)
        self.lsavings.setText("Savings")
        self.lsavings.setGeometry(20, 320, 150, 50)

        self.wsavings = QTextEdit(self)
        self.wsavings.setStyleSheet(tab_color)
        self.wsavings.setGeometry(100, 320, 150, 50)

            # Monthly spend
        self.lspend = QLabel(self)
        self.lspend.setText("Monthly\nspend")
        self.lspend.setGeometry(20, 390, 150, 50)

        self.wspend = QTextEdit(self)
        self.wspend.setStyleSheet(tab_color)
        self.wspend.setGeometry(100, 390, 150, 50)

            # Anual Projection
        self.lprojection = QLabel(self)
        self.lprojection.setText("Anual projection")
        self.lprojection.setGeometry(550, 10, 150, 50)

        self.wprojection = QTextEdit(self)
        self.wprojection.setStyleSheet(tab_color)
        self.wprojection.setGeometry(550, 45, 220, 430)

            # Short Term Projection
        self.lshort_term = QLabel(self)
        self.lshort_term.setText("Short Term")
        self.lshort_term.setGeometry(300, 10, 150, 50)

        self.wshort_term = QTextEdit(self)
        self.wshort_term.setStyleSheet(tab_color)
        self.wshort_term.setGeometry(300, 45, 220, 100)

        # set defaults
        self.weuros.setText('0.06')
        self.woutput.setText('500')
        self.wdays_month.setText('20')
        self.wsavings.setText('0')
        self.wspend.setText('0')

        self.show()


    def buttonClicked(self):
        #Get text in cells on left side
        per_word = self.weuros.toPlainText()
        output = self.woutput.toPlainText()
        days_in_month = self.wdays_month.toPlainText()
        savings = self.wsavings.toPlainText()
        spend = self.wspend.toPlainText()

        # Generate 1 float: daily total
        # Generate 1 int: monthly total
        # Generate 4 strings: Daily total, Weekly total, Monthly total and Monthly Output

        daily_total = int(output) * float(per_word)
        monthly_total = daily_total * int(days_in_month)

        daily_str = ('Daily total: ' + str(daily_total))
        weekly_str = ('Weekly total: ' + str(daily_total * int(days_in_month) / 4))
        monthly_str = ('Montly total: ' + str(monthly_total))
        monthly_output_str = ('Monthly output: ' + str(int(output) * int(days_in_month)))

        #Update short term cell
        short_term = str(daily_str + '\n' +
                        weekly_str + '\n' +
                        monthly_str + '\n\n'
                        + monthly_output_str)

        self.wshort_term.setText(short_term)

        #Update anual projection cell
        def get_forward_month_list():
            now = datetime.now()
            return [(now + relativedelta(months=i)).strftime('%b') for i in range(12)]

        def anual_projection(savings):
            projection_lst = []
            for i in get_forward_month_list():
                savings -= int(spend)
                savings += monthly_total
                projection_lst.append(i + ' ' + str(savings))

            projection_lst = '\n'.join(projection_lst)
            return projection_lst

        self.wprojection.setText(anual_projection(int(savings)))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Parse_GUI()
    sys.exit(app.exec_())